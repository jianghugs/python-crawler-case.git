# 1. 找到未加密的参数           #window.asrsea(参数xxx，参数xxx)
# 2. 先办法把参数进行加密（必须参考网页的逻辑） params => encText，encSecKey =>encSecKey
# 3. 请求网易云，拿到评论信息

from Crypto.Cipher import AES
from base64 import b64encode
import requests
import json

url = 'https://music.163.com/weapi/comment/resource/comments/get?csrf_token='
# 请求方式post

# 找到了解密的参数
data = {
    "csrf_token": "",
    "cursor": "-1",
    "offset": "0",
    "orderType": "1",
    "pageNo": "1",
    "pageSize": "20",
    "rid": "R_SO_4_1962305293",
    "threadId": "R_SO_4_1962305293"
}
f = "00e0b509f6259df8642dbc35662901477df22677ec152b5ff68ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee341f56135fccf695280104e0312ecbda92557c93870114af6c9d05c4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e82047b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7"
g = "0CoJUm6Qyw8W8jud"
e = '010001'
i = "dPblDGj1P1C92AfF"


def get_encSecKey():
    return "d3e9d403b81bae0c99c959e7bd7605168ba466ae405c4fb77b44c104d02a1bf9e7f1a4cf1ca433546b7305751664ddfb2b3f932e1e84a791e1ce20a7e5f655c31019918ff79900b5003f62650f8c7a3a5ca6ac2c29988e279db57504026c118fc1fd590e73f9e07928a23551bff2df1469b87633d141c4b6aee5dd221821b6e4"

def get_params(data):  # 默认这个地方接受到的是字符串
    first = enc_params(data, g)  # 第一次的结果
    second = enc_params(first, i)
    return second


def to_16(data):
    pad = 16 - len(data) % 16
    data += chr(pad) * pad
    return data


def enc_params(data, key):
    iv = '0102030405060708'
    data = to_16(data)
    # 创造加密器
    aes = AES.new(key=key.encode('utf-8'), IV=iv.encode('utf-8'), mode=AES.MODE_CBC)
    bs = aes.encrypt(data.encode('utf-8'))  # 加密，要求加密的内容的长度必须是16的倍数
    # 转换成字符串返回
    return str(b64encode(bs))


resp = requests.post(url=url, data={
    "params": get_params(json.dumps(data)),
    "encSecKey": get_encSecKey()
})

print(resp.text)
# 处理加密过程


"""
    function a(a = 16) {        #返回的随机16为字符
        var d, e, b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", c = "";
        for (d = 0; a > d; d += 1) #循环16次
            e = Math.random() * b.length,   #随机数
            e = Math.floor(e),      #取整
            c += b.charAt(e);       # 去字符串中的xxx位置
        return c
    }
    function b(a, b) {      #a是要加密的内容
        var c = CryptoJS.enc.Utf8.parse(b)  #b是密钥
          , d = CryptoJS.enc.Utf8.parse("0102030405060708")
          , e = CryptoJS.enc.Utf8.parse(a)  #e是数据
          , f = CryptoJS.AES.encrypt(e, c, {  #c加密的密钥
            iv: d,  # iv 偏移量
            mode: CryptoJS.mode.CBC  # 用的是cbc这个模式进行加密
        });
        return f.toString()
    }
    function c(a, b, c) {  # c里面不产出随机数
        var d, e;
        return setMaxDigits(131),
        d = new RSAKeyPair(b,"",c),
        e = encryptedString(d, a)
    }
    function d(d, e, f, g) { d:数据,  e:'010001',  f:很长，是一个定值 g:0CoJUm6Qyw8W8jud
        var h = {}  #空对象
          , i = a(16);  #i就是一个16为的随机值
        return h.encText = b(d, g),# g是密钥
        h.encText = b(h.encText, i),  #返回的就是params     i也是密钥
        h.encSecKey = c(i, e, f),   # 得到的就是encSecKey，e和f是固定的，如果此时我们把i固定，得到的key也是固定的
        h
    }
    两次加密：
    数据+g =>b => 第一次加密+i =>b =params
"""
