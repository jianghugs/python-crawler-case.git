import re
import smtplib  # 发邮件的模块
import time
from email.mime.text import MIMEText  # 定义邮件内容

import requests


def get_weather():
    # city = input("请输入需要查询城市的名称：\n")
    url = "http://api1.duozy.cn/api/qqtq.php"

    params = {
        # "msg": city
        # 需要查询的城市
        "msg": "重庆"
    }
    headers = {
        "Content-type": "application/x-www-form-urlencoded"
    }
    response = requests.get(url=url, params=params, headers=headers)
    str1 = response.text
    # 把字符串转换成列表。好调用
    a = re.findall(r"城市：(.*)", str1)
    d = re.findall(r"气温：(.*)", str1)
    e = re.findall(r"今日天气实况：(.*)", str1)
    f = re.findall(r"感冒指数：(.*)", str1)
    i = re.findall(r"运动指数：(.*)", str1)
    j = re.findall(r"过敏指数：(.*)", str1)
    k = re.findall(r"穿衣指数：(.*)", str1)
    lk = re.findall(r"洗车指数：(.*)", str1)
    m = re.findall(r"紫外线指数：(.*)", str1)
    data = [a, d, e, f, i, j, k, lk, m]
    # 返回一个字典类型。并且把服务器获取的时间也返回
    return data, response.headers['Date']


def sentemail():
    c = get_weather()
    now_date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    host = 'smtp.163.com'  # 设置发件服务器地址

    port = 465  # 设置发件服务器端口号。注意，这里有SSL和非SSL两种形式，现在一般是SSL方式

    sender = 'jianghugs@163.com'  # 设置发件邮箱，一定要自己注册的邮箱

    pwd = 'LZRAWDVQMPNHOQZQ'  # 设置发件邮箱的授权码密码，根据163邮箱提示，登录第三方邮件客户端需要授权码

    receiver = '1219649691@qq.com'  # 设置邮件接收人，可以是QQ邮箱

    body = '<p>城市：{0}</p><p>气温：{1}</p><p>今日天气实况：{2}</p><p>感冒指数：{3}</p><p>运动指数：{4}</p><p>过敏指数：{5}</p><p>穿衣指数：{' \
           '6}</p><p>洗车指数：{7}</p><p>紫外线指数：{8}</p><p>服务器相应实际时间：{9}</p>'.format(c[0][0], c[0][1], c[0][2], c[0][3],
                                                                              c[0][4], c[0][5], c[0][6],
                                                                              c[0][7], c[0][8], c[1])
    # 设置邮件正文，这里是支持HTML的
    msg = MIMEText(body, 'html')
    # 设置正文为符合邮件格式的HTML内容
    msg['subject'] = now_date + str(c[0][0]) + "天气情况"
    # 设置邮件标题
    msg['from'] = sender
    # 设置发送人
    msg['to'] = receiver
    # 设置接收人
    try:
        s = smtplib.SMTP_SSL(host, port)
        # 注意！如果是使用SSL端口，这里就要改为SMTP_SSL
        s.login(sender, pwd)
        # 登陆邮箱
        s.sendmail(sender, receiver, msg.as_string())
        # 发送邮件！
        print('邮件发送成功！\n')
        print('当然日期为{}'.format(now_date))
    except smtplib.SMTPException:
        print('邮箱发送失败！请重试！')


if __name__ == '__main__':
    while True:
        # 配置
        # __time_____
        # 定义每天十点钟执行程序
        ehour = 10  # 定时小时
        emin = 56  # 定时分钟
        esec = 0  # 定时秒
        current_time = time.localtime(time.time())  # 当前时间date
        # 操作
        if (current_time.tm_hour == ehour) and (current_time.tm_min == emin) and (current_time.tm_sec == esec):
            print("开始执行自动发送邮件功能......")
            # 调用相关方法：执行计算、发邮件动作。
            sentemail()
        time.sleep(1)
