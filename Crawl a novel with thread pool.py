import base64
import os
import time
import requests
from lxml import etree
from concurrent.futures import ThreadPoolExecutor

'''
使用多线程下载一篇478.7万小说
'''


def get_down_url(url):
    html = etree.HTML(requests.get(url).text)
    filename = html.xpath('/html/body/div[3]/div[1]/h1/text()')[0]
    if not os.path.exists(filename):
        os.mkdir(filename)
    # 获取所有的div
    div_list = html.xpath('/html/body/div[3]/div[2]/div[2]/div')
    # 使用线程池下载
    with ThreadPoolExecutor(1000) as t:
        for child_list in div_list:
            # 获取每个div的名字，方便给文件夹命名
            child_filename = child_list.xpath('./div/text()')[3].replace(' ', '_')
            # 获取每个列表中的url
            child_ul = child_list.xpath('./ul/li/a/@href')
            # print(child_ul)
            for x in range(len(child_ul)):
                if not os.path.exists(filename + '/' + child_filename):
                    os.mkdir(filename + '/' + child_filename)
                t.submit(download, child_ul[x], filename, child_filename)

                # 普通模式下载，数度极慢
                # download(child_ul[x],filename,child_filename)


def download(url, filename, child_filename):
    html = etree.HTML(requests.get(url).text)
    # 获取小说章节的名称
    title_filename = html.xpath('/html/body/div[2]/div[3]/div[3]/div/div[1]/div[2]/div[2]/text()')[0]
    # 获取小说内容
    content = html.xpath('/html/body/div[2]/div[3]/div[3]/div/div[1]/div[5]/p/text()')
    # 在对应部分下创建对应名称的txt文档
    f = open(filename + '/' + child_filename + '/' + title_filename + '.txt', 'w', encoding='utf-8')
    for c in content:
        f.write(c + '\n')
    print(f'{child_filename}{title_filename}下载完毕！！！')


def main():
    t1 = time.time()
    # 调用下载的函数，采用base64解码加密后的网站
    get_down_url(str(base64.b64decode('aHR0cDovL2Jvb2suem9uZ2hlbmcuY29tL3Nob3djaGFwdGVyLzEwODU5OTkuaHRtbA=='))[1:].replace("'",""))
    print(f'下载完成！一共耗时：{time.time() - t1} 秒！！')


if __name__ == '__main__':
    main()
