# coding=gbk
import os
import time

import requests
import json
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor


# 1分析目标网站，确定爬取的url路径，headers参数  https://haokan.baidu.com

def now_time():
    return int(str(time.time()).replace('.', '')[0:13])


def a(url, num):
    video = '下载视频'
    if not os.path.exists(video):
        os.mkdir(video)
    cookies = {
        'BIDUPSID': '70F4308021158815890A68857409D2FC',
        'PSTM': '1650766499',
        'BDUSS': 'm1PSnVzVEM4UjRwdWdXZzFZU3V5VDRBeEhyVGxTbHVhNVB-dUZBOH5BfmVBcDlpRVFBQUFBJCQAAAAAAAAAAAEAAADNkeo00MfI1m5pY2UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN51d2LedXdid',
        'BDUSS_BFESS': 'm1PSnVzVEM4UjRwdWdXZzFZU3V5VDRBeEhyVGxTbHVhNVB-dUZBOH5BfmVBcDlpRVFBQUFBJCQAAAAAAAAAAAEAAADNkeo00MfI1m5pY2UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN51d2LedXdid',
        'BAIDUID': '57368C4C8B794A858B0F58CDDA02305C:FG=1',
        'BDORZ': 'FFFB88E999055A3F8A630C64834BD6D0',
        'H_PS_PSSID': '',
        'ZD_ENTRY': 'baidu',
        'BAIDUID_BFESS': 'C98D2DF726812EA17CC893415F23F9DE:FG=1',
        'COMMON_LID': 'f47f76c05a5649fbfc551ebae9c4c7e8',
        'Hm_lvt_4aadd610dfd2f5972f1efee2653a2bc5': '1656471137,1656654318',
        'BDRCVFR[qm96YCg0y3b]': 'mk3SLVN4HKm',
        'delPer': '0',
        'ZFY': 'LJagOE0aV0i7SiZr44XwohxmrgQUMxJAwufWpzTzjmA:C',
        'hkpcvideolandquery': '%u63ED%u79D8%u4E49%u4E4C%u5C0F%u5546%u54C1%u6279%u53D1%u5E02%u573A%uFF0C%u51E0%u5757%u4E00%u4E2A%u7684%u53D1%u5149%u73A9%u5177%u4E00%u5929%u51FA%u51E0%u767E%u4E2A',
        'PC_TAB_LOG': 'video_details_page',
        'PSINO': '1',
        'Hm_lpvt_4aadd610dfd2f5972f1efee2653a2bc5': '1656752759',
        'ab_sr': '1.0.1_OWI4NzYwYzBkOTQyZGQ4OWIxNmJjMTBmOWVlOTlmN2UyNWM3ZGUzNmZlOTVkYjgzZjJjMTUwMDAyNGU1YTJiNzA4NzJiZTU3Njg3ZWMxNTNhODMxOTdkNjIzYjIwMDZiYjk4MzRlN2QzOTYwZGE0OTQ5OTY0N2NlODM1NTliZWY1YjZjYmE3OTNhNjQ1OTIxOGMzMTM2ZDg0YWU3NzYyZg==',
        'reptileData': '%7B%22data%22%3A%22284358074b76fa09a74495af9abb8d7f66b7256c77e68afcfa4c784d4d3d435b92b70edf1e8998656ac3b6f9e2167aebd66d8799f8b82cc4529d7c17fbf75a988ccee0805d8777f2a15a6dcd02cb57f27a8ad533681829d9fc9f2401e4b552ce%22%2C%22key_id%22%3A%2230%22%2C%22sign%22%3A%22516b1da5%22%7D',
        'ariaDefaultTheme': 'undefined',
        'av1_switch_v3': '0',
        'RT': '"z=1&dm=baidu.com&si=ljtaazui6c8&ss=l53ntaxf&sl=1&tt=4pa&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf&ld=5pg&ul=86p&hd=969"',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Referer': 'https://haokan.baidu.com/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = {
        'tab': 'recommend',
        'act': 'pcFeed',
        'pd': 'pc',
        'num': num,
        'shuaxin_id': now_time()
    }

    resp = requests.get(url=url, params=params, cookies=cookies, headers=headers)
    # 2发送请求，获取响应数据
    # resp = requests.get(url=url, headers=headers)
    raw_json = resp.text
    # print(raw_json)
    # comments=json.loads(raw_json[20:-2])['play_url']
    # comments
    # 3解析数据
    # 3.1数据转换
    do = json.loads(raw_json)
    # # 3.2数据解析
    data_list = do['data']['response']['videos']
    for data in data_list:
        # print(data)
        video_title = data["title"] + ".mp4"  # 视频文件名
        video_url = data['play_url']  # 视频的url地址
        # print("视频名称为：{video_title}\nURL地址为：{video_url}".format(video_title=video_title, video_url=video_url))

        # 4保存数据
        # 再次发送请求
        print("正在下载：", video_title)
        video_data = requests.get(video_url, headers=headers)
        with open(video + '/' + video_title, "wb") as f:  # 视频是二进制，所以需要用到wb写入方式
            f.write(video_data.content)
            print(f"{video_title}:下载完成！\n")


if __name__ == '__main__':
    url = 'https://haokan.baidu.com/web/video/feed'
    with ThreadPoolExecutor(100) as t:  # 开始100个线程主
        for num in range(1, 1000):
            t.submit(a, url, num)
